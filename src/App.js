import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      region: ''
    };
  }

  // TODO: change this implementation from static to dynamic
  buildLocationRows(locations) {
    let result = [];
    Object.keys(locations).forEach(key => {
      let location = locations[key];
      result.push(<tr key={location.name}><td>{location.name}</td><td>{location.region}</td></tr>)
    });
    return result;
  };

  inputValueChange = (name, evt) => {
    this.setState({
     [name]: evt.target.value
    });
  }

  addLocation = () => {
    this.props.addLocation({ name: this.state.name, region: this.state.region });
  }
  
  // TODO: change the implementation of the add_location button to retrieve the name and region via form input elements
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to Elminda</h2>
        </div>

        <div >
          <div className="Input-fields">
            <label className="Input-field">
              Name:
              <input type="text" name="name" onChange={evt => this.inputValueChange("name", evt)}/>
            </label>
            <label className="Input-field">
              Region:
              <input type="text" name="region" onChange={evt => this.inputValueChange("region", evt)}/>
            </label>
        </div>
      <button id="add_location"
        onClick={this.addLocation}
        >
        Add Name and Region
      </button>
      </div>
      <table>
        <thead>
          <tr><th>Name</th><th>Region</th></tr>
        </thead>
        <tbody>
          { this.buildLocationRows(this.props.locations) }
        </tbody>
      </table>
      </div>
    );
  }
}

export default App;
